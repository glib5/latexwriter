# latexWriter

simple classe to create .tex files

## Description

Basically a wrapper around file.write() for .tex files. 

## Usage
Use it to write LaTeX code to a file, then you must have a TeX distribution in order to create the .pdf file.

# example
the code produces the attached pdf file, once compiled using a LaTeX distribution

## Project status
Working but really basic. All methods that come to mind could be a nice addition 

