from dataclasses import dataclass
from pathlib import Path
from typing import Iterable


@dataclass(frozen=True)
class Preamble:
    article:str = r'''% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

% This is a simple template for a LaTeX document using the "article" class.
% See "book", "report", "letter" for other types of document.

\documentclass[11pt]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% Examples of Article customizations
% These packages are optional, depending whether you want the features they provide.
% See the LaTeX Companion or other references for full information.

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....
% \geometry{margin=2in} % for example, change the margins to 2 inches all round
% \geometry{landscape} % set up the page for landscape
%   read geometry.pdf for detailed page layout information

\usepackage{graphicx} % support the \includegraphics command and options

% \usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
% These packages are all incorporated in the memoir class to one degree or another...

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\rmfamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

%%% END Article customizations

%%% The "real" document content comes below...

\title{Brief Article}
\author{The Author}
%\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 

\begin{document}
\maketitle
'''
    beamer:str = r'''\documentclass{beamer}
\usetheme{Hannover} %Hannover, default, CambridgeUS
%boadilla, madrid, Warsaw, boxes, CambridgeUS, default, Goettingen, Hannover, Marburg, Montpellier
% i like these, use internet for others

\usepackage[italian]{babel}
\usepackage[utf8]{inputenc}
\usepackage{translator}

\title{Title}
\subtitle{subtitle}
\author{author}
\institute{uni}
\date{date}
% If you have a file called "university-logo-filename.xxx", where xxx
% is a graphic format that can be processed by latex or pdflatex,
% resp., then you can add a logo as follows:
% \pgfdeclareimage[height=0.5cm]{university-logo}{university-logo-filename}
% \logo{\pgfuseimage{university-logo}}

%%%PAGE NUMBER
\addtobeamertemplate{navigation symbols}{}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    \insertframenumber/\inserttotalframenumber
}

%%%OUTLINE
%% personal comment: use only \section{} and keep track of eventual subsections with frame titles
%% due to the double command, avoid having section and subsection with no frame in between
\AtBeginSection[]
{
    \begin{frame}
        \frametitle{recap section}
        \tableofcontents[currentsection]
    \end{frame}
}

%\AtBeginSubsection[] %%if there are too many recaps, remove THIS block
%{
%    \begin{frame}
%        \frametitle{recap subsection}
%        \tableofcontents[currentsection, currentsubsection]
%    \end{frame}
%}

\usepackage{amssymb} %for complex math symbols
\usepackage{amsmath} % for unnumbered equations
\usepackage{pgfplots} %necessary to PLOT
\usepackage{xcolor}%colori
%\usepackage{multicol}%%\begin{multicols}{2}%%\end{multicols}
\usepackage{amsthm} %for math environment
\usepackage{graphicx} % support the \includegraphics
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths

\begin{document}
'''


class TeXWriter:

    def __init__(self, file:Path, article:bool=True) -> None:
        assert isinstance(file, Path)
        assert isinstance(article, bool)
        assert file.suffix == ".tex"
        self._f = file
        self._article = article

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} object with file: {str(self._f)}"

    def __enter__(self):
        # this creates a fresh new file
        self._f.write_text("")
        preamble = Preamble.article if self._article else Preamble.beamer
        self.write(preamble)
        return self
    
    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.write("\n\\end{document}")

    def write(self, text):
        """writes unformatted text to tex file"""
        assert isinstance(text, str)
        with self._f.open(mode="a", newline="\n") as f:
            f.write(text)

    def section(self, text:str, level:int=1):
        """writes a section"""
        assert isinstance(text, str)
        assert isinstance(level, int)
        assert level in (1, 2, 3)
        if level == 1: 
            section = "\n\\section{%s}\n"%text
        elif level == 2: 
            section = "\n\\subsection{%s}\n"%text
        elif level == 3: 
            section = "\n\\subsubsection{%s}\n"%text
        self.write(section)

    def table(self, table:Iterable[Iterable]):
        """writes a 2D table to tex file. full grid, centered"""
        assert isinstance(table, Iterable)
        ncols = 0
        for line in table:
            assert isinstance(line, Iterable)
            if not ncols:
                ncols = len(line)
                assert ncols > 0
            assert len(line) == ncols
            for element in line:
                assert isinstance(element, (int, float, str))
        # is a good 2D table
        self.write("\n\\begin{center}\n")
        table_structure = "|" + "".join("c|" for _ in range(ncols))
        self.write("\\begin{tabular}{%s}\n"%table_structure)        
        for line in table:
            self.write("\\hline\n")
            for count, element in enumerate(line, start=1):
                text = "%s &"%str(element) if count != ncols else "%s \\\\ \n"%str(element)
                self.write(text=text)
        self.write("\\hline \n")
        self.write("\\end{tabular}\n")
        self.write("\\end{center}\n")
        
    def block(self, block_type:str, method:str, *args):
        """
        creates a block and inside of it uses one of the other writing methods

        Args:
            block type: the block that begins and ends
            method: the name of the method to use
            *args will be passed to the specified method
        """

        assert isinstance(block_type, str)
        assert isinstance(method, str)
        assert hasattr(self, method)
        self.write("\n\\begin{%s}\n"%block_type)
        func = getattr(self, method)
        func(*args)
        self.write("\n\\end{%s}\n"%block_type)

    def display_tex(self):
        """shows the .tex file content -- for debugging"""
        with self._f.open() as f:
            for line in f:
                print(line)

    def itemize(self, items:Iterable[str], numbered:bool=False):
        """writes an itemize block"""
        assert isinstance(items, Iterable)
        assert all(isinstance(i, str) for i in items)
        assert isinstance(numbered, bool)
        block_type = "enumerate" if numbered else "itemize"
        self.write("\n\\begin{%s}\n"%block_type)
        for i in items:
            item = f"\\item {i}\n"
            self.write(item)
        self.write("\\end{%s}\n"%block_type)


def ex():

    tex = Path(__file__).with_name("example.tex")

    with TeXWriter(file=tex) as article:
        # repr
        print(article)
        # write basic text
        article.write("Example!")
        # add a scetion
        article.section(text="some section", level=2)
        # basic 2D tables
        tab = ((1, 2), (3, 4))
        article.table(table=tab)
        # code blocks
        article.block("center", "write", "centerd text")
        # nested code blocks
        # outer block is "center", inner one is "large", inside these some text will be written
        article.block("center", "block", "large", "write", "some centered and large text")
        # pointed list
        fruits = ["apple", "banana", "cherry"]
        article.itemize(items=fruits, numbered=True)
        animals = ("lion", "elephant", "dolphin")
        article.itemize(items=animals)
        # for debugging
        # article.display_tex()


if __name__ == "__main__":
    ex()